variable "prefix" {
  default = "adcloud"
}

variable "project" {
  default = "presto-app-api-devops"
}

variable "contact" {
  default = "anandan.subbiah@hcl.com"
}
